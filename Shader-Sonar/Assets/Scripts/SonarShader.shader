﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/SonarShader"
{
	Properties
	{
		//The values that are in RANGE is because they look garrish beyond those values		
		_ScanWidth("Scan Width", float) = 10
		_LeadSharp("Edge Relevance", Range(0.01, 5.0)) = 1.0
		_LeadColor("Edge Color", Color) = (1, 1, 1, 0)
		_MidColor("Mid Color", Color) = (1, 1, 1, 0)
		_TrailColor("Trail Color", Color) = (1, 1, 1, 0)
		_HBarColor("Horizontal Bar Color", Color) = (0.5, 0.5, 0.5, 0)		
		_HBarWidth("Horizontal Bar Width", Range(100.0, 500.0)) = 100.0
		_HBarBuffer("Horizontal Bar Buffer", Range(1.0, 5.0)) = 2.0
	}
		SubShader
	{
		// No culling or depth, that is done in the script
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Cull Off
		ZWrite Off
		ZTest Always

		//Only one pass needed
		Pass
	{
		CGPROGRAM

#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

		struct VertIn//The viewing camera texture
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
		float4 ray : TEXCOORD1;
	};

	struct VertOut//The modified texture
	{
		float4 vertex : SV_POSITION;
		float2 uv : TEXCOORD0;
		float2 uv_depth : TEXCOORD1;
		float4 interpolatedRay : TEXCOORD2;
	};

	float4 _MainTex_TexelSize;
	float4 _CameraWS;

	VertOut vert(VertIn v)
	{
		VertOut o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv.xy;
		o.uv_depth = v.uv.xy;

	#if UNITY_UV_STARTS_AT_TOP
		if (_MainTex_TexelSize.y < 0)
			o.uv.y = 1 - o.uv.y;
	#endif		
		o.interpolatedRay = v.ray;
		return o;
	}

	sampler2D _MainTex;
	sampler2D_float _CameraDepthTexture;
	float4 _WorldSpaceScannerPos;
	float _ScanDistance;
	float _ScanWidth;
	float _LeadSharp;
	float4 _LeadColor;
	float4 _MidColor;
	float4 _TrailColor;
	float4 _HBarColor;
	float _HBarWidth;
	float _HBarBuffer;
	float _Alpha;
	int _Bars;


	float4 horizBars(float2 p)//Horizontal bars, VCR-style
	{
		if (_Bars == 0)
		{
			return 0;
		}
		else
		{
			return 1 - saturate(round(abs(frac(p.y * _HBarWidth) * _HBarBuffer)));
		}
	}	

	half4 frag(VertOut i) : SV_Target
	{
		half4 col = tex2D(_MainTex, i.uv);

		float rawDepth = DecodeFloatRG(tex2D(_CameraDepthTexture, i.uv_depth));//The Depth texture of the camera viewing as values
		float linearDepth = Linear01Depth(rawDepth);//Translates the values to 0-255 RGB values
		float4 wsDir = linearDepth * i.interpolatedRay;
		float3 wsPos = _WorldSpaceCameraPos + wsDir;
		half4 sonnarCol = half4(0, 0, 0, 0);

		float alpha = tex2D(_MainTex,float2(i.uv.x,i.uv.y));

		float dist = distance(wsPos, _WorldSpaceScannerPos);

		if (dist < _ScanDistance && dist > _ScanDistance - _ScanWidth && linearDepth < 1 && _Alpha > 0)//Do it until either no more terrain or out of circle bounds
		{
			float diff = 1 - (_ScanDistance - dist) / (_ScanWidth);//Create a diffuse FX based on the distance and the width

			half4 edge = lerp(_MidColor, _LeadColor, pow(diff, _LeadSharp));//The "edge" of the circle is a mix 
																				 //of both the middle color and the lead color, with a diffused "main" edge color
			
			sonnarCol = lerp(_TrailColor, edge, diff) + horizBars(i.uv) *_HBarColor; //the color of the sonar is determined by the main Trail Color
																					 // plus the "edge" of the circle and 
																					 //Here I also add the horizontal bars and their color
			sonnarCol *= diff;					
			
			alpha *= _Alpha;//1 is the full texture, and 0 is invisible, so that's what what's happenning. A gradual descent into a null texture

			sonnarCol *= alpha;
		}
		
		return col + sonnarCol;		
	}
		ENDCG
	}
	}
}