﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SonarScript : MonoBehaviour
{
    public Transform ScannerOrigin;
    public Material EffectMaterial;
    public float ScanSpeed;

    [Range(0.1f, 2.0f)]
    public float FadeSpeed;

    [Range(0,1)]
    public int BarsOn;

    private float ScanDistance;
    private Camera _camera;
    private float Alpha;
    private float initialAlpha = 1;

    bool _scanning;

    void Start()
    {
        Alpha = initialAlpha;
    }

    void Update()
    {
        if (_scanning)
        {
            if (ScanDistance <= _camera.farClipPlane)//Do it until the camera wont show it
            {
                ScanDistance += Time.deltaTime * ScanSpeed;
            }
            else
            {
                Alpha = initialAlpha;
                ScanDistance = 0;
                _scanning = false;                
            }

            if (Alpha >= 0)
            {
                Alpha -= Time.deltaTime*FadeSpeed;
            }
            else
            {
                Alpha = initialAlpha;
                ScanDistance = 0;
                _scanning = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            _scanning = true;
            ScanDistance = 0;
            Alpha = initialAlpha;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                _scanning = true;
                ScanDistance = 0;
                Alpha = initialAlpha;
                ScannerOrigin.position = hit.point;
            }
        }
    }

    void OnEnable()
    {
        _camera = GetComponent<Camera>();
        _camera.depthTextureMode = DepthTextureMode.Depth;//Generate a Depth texture for the terrain, if there isn't one
    }

   [ImageEffectOpaque]//This is necesary so it applies the modified texture at the circle actual position before the transparencies
                       //so the circle doesnt look like opaque circles and have actual transparencies
    void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        //Set the shader AoE and apply it
        EffectMaterial.SetVector("_WorldSpaceScannerPos", ScannerOrigin.position);
        EffectMaterial.SetFloat("_ScanDistance", ScanDistance);
        EffectMaterial.SetFloat("_Alpha", Alpha);
        EffectMaterial.SetInt("_Bars", BarsOn);
        GenerateCameraTexture(src, dst, EffectMaterial);
    }
    
    void GenerateCameraTexture(RenderTexture source, RenderTexture dest, Material mat)
    {
        //It receives the texture created by the camera, 
        //the texture it will apply to the camera and the 
        //material with the shader, and generates a new 
        //texture based on the current viewing of the camera
        //which then is the one displayed on OnRenderImage

        // Compute Frustum Corners
        float camFar = _camera.farClipPlane;
        float camFov = _camera.fieldOfView;
        float camAspect = _camera.aspect;

        float fovWHalf = camFov * 0.5f;

        Vector3 toRight = _camera.transform.right * Mathf.Tan(fovWHalf * Mathf.Deg2Rad) * camAspect;
        Vector3 toTop = _camera.transform.up * Mathf.Tan(fovWHalf * Mathf.Deg2Rad);

        Vector3 topLeft = (_camera.transform.forward - toRight + toTop);
        float camScale = topLeft.magnitude * camFar;

        topLeft.Normalize();
        topLeft *= camScale;

        Vector3 topRight = (_camera.transform.forward + toRight + toTop);
        topRight.Normalize();
        topRight *= camScale;

        Vector3 bottomRight = (_camera.transform.forward + toRight - toTop);
        bottomRight.Normalize();
        bottomRight *= camScale;

        Vector3 bottomLeft = (_camera.transform.forward - toRight - toTop);
        bottomLeft.Normalize();
        bottomLeft *= camScale;

        // Custom Blit, encoding Frustum Corners as additional Texture Coordinates
        RenderTexture.active = dest;

        mat.SetTexture("_MainTex", source);

        GL.PushMatrix();
        GL.LoadOrtho();

        mat.SetPass(0);

        GL.Begin(GL.QUADS);

        GL.MultiTexCoord2(0, 0.0f, 0.0f);
        GL.MultiTexCoord(1, bottomLeft);
        GL.Vertex3(0.0f, 0.0f, 0.0f);

        GL.MultiTexCoord2(0, 1.0f, 0.0f);
        GL.MultiTexCoord(1, bottomRight);
        GL.Vertex3(1.0f, 0.0f, 0.0f);

        GL.MultiTexCoord2(0, 1.0f, 1.0f);
        GL.MultiTexCoord(1, topRight);
        GL.Vertex3(1.0f, 1.0f, 0.0f);

        GL.MultiTexCoord2(0, 0.0f, 1.0f);
        GL.MultiTexCoord(1, topLeft);
        GL.Vertex3(0.0f, 1.0f, 0.0f);

        GL.End();
        GL.PopMatrix();

        //The "first" pass only generates the "Camera View" Texture, but since it's a loop, the next pass recognizes the texture and begins the FX
    }
}
